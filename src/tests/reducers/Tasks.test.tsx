import { TaskRudcer } from "../../reducers/Tasks";
import Tasks from "../fixtures/tasks";
import { TaskActionTypes } from "../../types/actions";

test("should remove task by id", () => {
  const action: TaskActionTypes = {
    type: "REMOVE_TASK",
    id: Tasks[0].id
  };

  const state = TaskRudcer(Tasks, action);
  expect(state).toEqual([Tasks[1], Tasks[2]]);
});

test("should not remove task if id not found", () => {
  const action: TaskActionTypes = {
    type: "REMOVE_TASK",
    id: "-1"
  };

  const state = TaskRudcer(Tasks, action);
  expect(state).toEqual(Tasks);
});

test("should add task with provided values", () => {
  const task = {
    id: "3",
    description: "blabla",
    category: "work"
  };

  const action: TaskActionTypes = {
    type: "ADD_TASK",
    task
  };

  const state = TaskRudcer(Tasks, action);
  expect(state).toEqual([...Tasks, task]);
});
