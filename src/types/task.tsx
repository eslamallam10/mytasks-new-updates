export interface Task {
  id: string;
  category: string;
  description: string;
  createdAt?: any;
}
