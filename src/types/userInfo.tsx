export interface UserProfileData {
  name?: string;
  email?: string;
  isLoggedin?: boolean;
}
