import { Task } from "./task";

//--------------------------
//  Filters action
//--------------------------

export const START_DATE = "START_DATE";

interface setStartDate {
  type: typeof START_DATE;
  startDate: string;
}

//--------------------------
//  Auth action
//--------------------------

export const GET_USER_INFO = "GET_USER_INFO";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

interface getUserInfo {
  type: typeof GET_USER_INFO;
}

interface login {
  type: typeof LOGIN;
}

interface logout {
  type: typeof LOGOUT;
}

//--------------------------
//  Task action
//--------------------------

export const REMOVE_TASK = "REMOVE_TASK";
export const ADD_TASK = "ADD_TASK";

export interface removeTaskAction {
  type: typeof REMOVE_TASK;
  id: string;
}

export interface addNewTaskAction {
  type: typeof ADD_TASK;
  task: Task;
}

export type FilterActionTypes = setStartDate;
export type AuthActionTypes = getUserInfo | login | logout;
export type TaskActionTypes = removeTaskAction | addNewTaskAction;

export type AppActions = TaskActionTypes | AuthActionTypes | FilterActionTypes;
