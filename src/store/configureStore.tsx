import { createStore, combineReducers } from "redux";
import { TaskRudcer } from "../reducers/Tasks";
import { AuthReducer } from "../reducers/Auth";
import { FiltersReducer } from "../reducers/Filter";
import { reducer as formReducer } from "redux-form";

const rootReducer = combineReducers({
  tasks: TaskRudcer,
  auth: AuthReducer,
  filters: FiltersReducer,
  form: formReducer
});

export type RootState = ReturnType<typeof rootReducer>;

export const store = createStore(rootReducer);
