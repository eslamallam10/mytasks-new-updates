import MyTasks from "../Data/Tasks.json";
import { TaskActionTypes } from "../types/actions";
import { Task } from "../types/task";

const tasksDefaultState: Task[] = MyTasks;

export const TaskRudcer = (
  state = tasksDefaultState,
  action: TaskActionTypes
): Task[] => {
  switch (action.type) {
    case "ADD_TASK":
      return [...state, action.task];
    case "REMOVE_TASK":
      return state.filter(({ id }) => id !== action.id);

    default:
      return state;
  }
};
