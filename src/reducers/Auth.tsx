import userInfo from "../Data/ProfileInfo.json";
import { AuthActionTypes } from "../types/actions";
import { UserProfileData } from "../types/userInfo";

const AuthDefaultState: UserProfileData = userInfo;

export const AuthReducer = (
  state = AuthDefaultState,
  action: AuthActionTypes
): UserProfileData => {
  switch (action.type) {
    case "GET_USER_INFO":
      return userInfo;
    case "LOGIN":
      return {
        ...state,
        isLoggedin: true
      };
    case "LOGOUT":
      return {
        isLoggedin: false
      };
    default:
      return state;
  }
};
