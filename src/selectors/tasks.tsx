import moment from "moment";
import { Task } from "../types/task";

export const getTasksOfToday = (tasks: Task[], date: moment.Moment) => {
  return tasks.filter(task => {
    const createdAt = moment(task.createdAt);
    const startDateMatch = date ? date.isSame(createdAt, "day") : true;

    return startDateMatch;
  });
};
