import { AppActions } from "../types/actions";

export const getUserInfo = (): AppActions => ({
  type: "GET_USER_INFO"
});

export const login = (): AppActions => ({
  type: "LOGIN"
});

export const logout = (): AppActions => ({
  type: "LOGOUT"
});
