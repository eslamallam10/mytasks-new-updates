// SET_START_DATE
export const setStartDate = (startDate: string) => ({
  type: "START_DATE",
  startDate
});
