import uuid from "uuid";
import { AppActions } from "../types/actions";
import { Task } from "../types/task";

// ADD_TASK
export const addNewTask = (task: Task): AppActions => ({
  type: "ADD_TASK",
  //task
  task: {
    id: uuid(),
    category: task.category,
    description: task.description,
    createdAt: task.createdAt
  }
});

// REMOVE_TASK
export const removeTask = (id: string): AppActions => ({
  type: "REMOVE_TASK",
  id
});
