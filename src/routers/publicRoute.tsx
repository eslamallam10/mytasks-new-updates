import React from "react";
import { connect } from "react-redux";
import { Route, Redirect } from "react-router-dom";
import { RootState } from "../store/configureStore";

export const PublicRoute = ({
  isAuthenticated,
  component: Component,
  ...rest
}: any) => {
  return (
    <Route
      {...rest}
      component={(props: any) =>
        isAuthenticated ? (
          <div>
            <Redirect to="/profile" />
          </div>
        ) : (
          <Component {...props} />
        )
      }
    />
  );
};

const mapStateToProps = (state: RootState) => ({
  isAuthenticated: state.auth.isLoggedin
});

export default connect(mapStateToProps)(PublicRoute);
