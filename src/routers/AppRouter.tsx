import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import Home from "../components/Home";
import TasksList from "../components/Task/TasksList";
import Login from "../components/Login/Login";
import Profile from "../components/Profile";
import TaskForm from "../components/Task/TaskForm";
import PrivateRoute from "./privateRoute";
import PublicRoute from "./publicRoute";

const AppRourer = () => {
  return (
    <BrowserRouter>
      <div>
        <Switch>
          <PublicRoute path="/" component={Login} exact={true} />
          <PublicRoute path="/login" component={Login} />
          <PrivateRoute path="/home" component={Home} />
          <PrivateRoute path="/profile" component={Profile} />
          <PrivateRoute path="/tasks" component={TasksList} />
          <PrivateRoute path="/createtask" component={TaskForm} />
        </Switch>
      </div>
    </BrowserRouter>
  );
};

export default AppRourer;
