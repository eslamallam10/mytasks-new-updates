import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import DatePicker, { normalizeDates, formatDates } from "../DatePicker";
import submitFilter from "./submitFilter";
import "../../styles/listFilter.css";

const TasksListFilter = (props: InjectedFormProps) => {
  const { handleSubmit, submitting } = props;
  return (
    <form onSubmit={handleSubmit(submitFilter)}>
      <div>
        <label>Filter By Date</label>
        <Field
          name="createdAt"
          component={DatePicker}
          placeholder="Pick date"
          parse={normalizeDates}
          format={formatDates}
        />
        <button className="filter-btn" type="submit" disabled={submitting}>
          Filter
        </button>
      </div>
    </form>
  );
};

export default reduxForm({
  form: "TasksFilterForm",
  onSubmit: submitFilter
})(TasksListFilter);
