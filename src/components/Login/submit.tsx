import * as React from "react";
import { SubmissionError } from "redux-form";
import { Redirect } from "react-router-dom";
import { Dispatch } from "redux";
import { login, getUserInfo } from "../../actions/Auth";
import ax from "axios";

const submit = (values: any, dispatch: Dispatch) => {
  return ax.get("./UserCredentials.json").then(res => {
    if (!values.username || !values.password) {
      throw new SubmissionError({
        username: "Please enter your credentials",
        _error: "Login failed!"
      });
    } else if (res.data[0].username !== values.username) {
      throw new SubmissionError({
        username: "User does not exist",
        _error: "Login failed!"
      });
    } else if (res.data[0].password !== values.password) {
      throw new SubmissionError({
        password: "Wrong password",
        _error: "Login failed!"
      });
    } else {
      dispatch(getUserInfo());
      dispatch(login());
      return <Redirect to="/profile" />;
    }
  });
};

export default submit;
