import React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { logout } from "../actions/Auth";
import "../styles/header.css";

interface IProps {
  dispatch: Dispatch;
}

const Header = ({ dispatch }: IProps) => {
  return (
    <header>
      <NavLink className="nav-item" to="/home" activeClassName="is-active">
        Home
      </NavLink>
      <NavLink className="nav-item" to="/tasks" activeClassName="is-active">
        Tasks
      </NavLink>
      <NavLink className="nav-item" to="/profile" activeClassName="is-active">
        Profile
      </NavLink>
      <button
        onClick={() => {
          dispatch(logout());
        }}
      >
        Log Out
      </button>
    </header>
  );
};

export default connect(null)(Header);
