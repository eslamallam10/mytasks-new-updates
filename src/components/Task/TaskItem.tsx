import React from "react";
import { connect } from "react-redux";
import { removeTask } from "../../actions/Task";
import "../../styles/tasks.css";
import { Dispatch } from "redux";
import { AppActions } from "../../types/actions";

interface IProps {
  id: string;
  category: string;
  description: string;
  dispatch: Dispatch<AppActions>;
}

const TaskItem = ({ id, category, description, dispatch }: IProps) => {
  return (
    <div className="task-item">
      <h2>{description}</h2>
      <span>category: {category}</span>{" "}
      <button
        onClick={() => {
          dispatch(removeTask(id));
        }}
      >
        Remove
      </button>
    </div>
  );
};

export default connect(null)(TaskItem);
