import { SubmissionError } from "redux-form";
import { Dispatch } from "redux";
import { addNewTask } from "../../actions/Task";

const submitTask = (values: any, dispatch: Dispatch) => {
  if (!values.category) {
    throw new SubmissionError({
      category: "Please enter Category!"
    });
  } else if (!values.description) {
    throw new SubmissionError({
      category: "Please enter description!"
    });
  } else {
    const taskValue = {
      id: values.id,
      category: values.category,
      description: values.description,
      createdAt: values.createdAt
    };

    dispatch(addNewTask(taskValue));
  }
};

export default submitTask;
