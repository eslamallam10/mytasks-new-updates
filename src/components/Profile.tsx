import React from "react";
import { connect } from "react-redux";
import { RootState } from "../store/configureStore";
import "../styles/profile.css";

export const Profile = (props: RootState) => {
  const { name, email } = props.auth;

  return (
    <div className="profile-box">
      <h1>Welcome</h1>
      <h4>Don't Share your data with anyone</h4>
      <p>Name: {name}</p>
      <p>E-mail: {email}</p>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  auth: state.auth
});

export default connect(mapStateToProps, null)(Profile);
